# I have to declare this unaliased or stuff breaks
# https://github.com/hashicorp/terraform/issues/21330
provider "aws" {
  version = "~> 2.0"
  region  = var.aws_region
}

provider "cloudflare" {
  version = "~> 2.0"
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

resource "aws_s3_bucket" "site_bucket" {
  bucket = var.site_domain
  acl    = "public-read"
  region = var.aws_region

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  versioning {
    enabled = false
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "POST", "PUT", "HEAD", "DELETE"]
    allowed_origins = ["http://${var.site_domain}", "https://${var.site_domain}"]
    expose_headers  = ["ETag", "x-amz-meta-custom-header"]
    max_age_seconds = 0 # Should this be 3000?
  }
  tags = {
    site = var.site_domain
  }
}

resource "aws_s3_bucket" "site_staging_bucket" {
  bucket = var.site_staging_domain
  acl    = "public-read"
  region = var.aws_region

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  versioning {
    enabled = false
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "POST", "PUT", "HEAD", "DELETE"]
    allowed_origins = ["http://${var.site_staging_domain}", "https://${var.site_staging_domain}"]
    expose_headers  = ["ETag", "x-amz-meta-custom-header"]
    max_age_seconds = 0 # Should this be 3000?
  }
  tags = {
    site = var.site_staging_domain
  }
}

resource "aws_s3_bucket_policy" "site_bucket" {
  bucket = aws_s3_bucket.site_bucket.id
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "s3:GetObject"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "arn:aws:s3:::${var.site_domain}/*"
          Sid       = "PublicReadGetObject"
          Condition = {
            IpAddress = {
              "aws:SourceIp" = var.cloudflare_ip_addresses
            }
          }
        },
      ]
      Version = "2012-10-17"
    }
  )
}

resource "aws_s3_bucket_policy" "site_staging_bucket" {
  bucket = aws_s3_bucket.site_staging_bucket.id
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "s3:GetObject"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "arn:aws:s3:::${var.site_staging_domain}/*"
          Sid       = "PublicReadGetObject"
          Condition = {
            IpAddress = {
              "aws:SourceIp" = var.cloudflare_ip_addresses
            }
          }
        },
      ]
      Version = "2012-10-17"
    }
  )
}

resource "cloudflare_zone" "site_zone" {
  zone = var.site_domain
  plan = var.cloudflare_zone_plan
  type = var.cloudflare_zone_type
}

resource "cloudflare_zone_settings_override" "site_zone_settings_override" {
  zone_id = cloudflare_zone.site_zone.id
  settings {
    always_online            = "on"
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    browser_cache_ttl        = 14400
    browser_check            = "on"
    cache_level              = "aggressive"
    edge_cache_ttl           = 7200
    email_obfuscation        = "on"
    ip_geolocation           = "on"
    ipv6                     = "on"
    min_tls_version          = "1.0"
    minify {
      css  = "off"
      html = "off"
      js   = "off"
    }
    mobile_redirect {
      mobile_subdomain = ""
      status           = "off"
      strip_uri        = false
    }
    security_header {
      enabled            = false
      include_subdomains = false
      max_age            = 0
      nosniff            = false
      preload            = false
    }
    ssl             = "flexible"
    tls_1_3         = "on"
    tls_client_auth = "off"
  }
}

resource "cloudflare_record" "site_cname" {
  zone_id = cloudflare_zone.site_zone.id
  name    = var.site_domain
  type    = "CNAME"
  ttl     = "1"
  proxied = "true"
  value   = aws_s3_bucket.site_bucket.website_endpoint
}

resource "cloudflare_record" "site_cname_www" {
  zone_id = cloudflare_zone.site_zone.id
  name    = "www"
  type    = "CNAME"
  ttl     = "1"
  proxied = "true"
  value   = aws_s3_bucket.site_bucket.website_endpoint
}

# This assumes your staging site is a subdomain of the main site.
# This kit doesn't really work otherwise.
resource "cloudflare_record" "site_cname_staging" {
  zone_id = cloudflare_zone.site_zone.id
  name    = "${var.site_staging_subdomain}"
  type    = "CNAME"
  ttl     = "1"
  proxied = "true"
  value   = "${aws_s3_bucket.site_staging_bucket.website_endpoint}"
}
