output "site_bucket_url" {
  value       = aws_s3_bucket.site_bucket.website_endpoint
  description = "URL for the staging site S3 bucket"
}

output "site_staging_bucket_url" {
  value       = aws_s3_bucket.site_staging_bucket.website_endpoint
  description = "URL for the staging site S3 bucket"
}

output "cloudflare_zone_id" {
  value       = cloudflare_zone.site_zone.id
  description = "Cloudflare zone id"
}

output "cloudflare_record_site_cname_record_id" {
  value       = cloudflare_record.site_cname.id
  description = "Record id for the Cloudflare site primary CNAME"
}

output "cloudflare_record_site_cname_www_record_id" {
  value       = cloudflare_record.site_cname_www.id
  description = "Record id for the Cloudflare site www CNAME"
}

output "cloudflare_record_site_cname_staging_record_id" {
  value       = cloudflare_record.site_cname_staging.id
  description = "Record id for the Cloudflare site staging CNAME"
}

