variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "site_domain" {
  type = string
}

variable "site_staging_subdomain" {
  type    = string
  default = "staging"
}

variable "site_staging_domain" {
  type = string
}

variable "cloudflare_email" {
  type = string
}

variable "cloudflare_api_key" {
  type = string
}

variable "cloudflare_zone_plan" {
  type    = string
  default = "free"
}

# Use 'full' if you've pointed your domain at Cloudflare's name servers
variable "cloudflare_zone_type" {
  type    = string
  default = "full"
}

# https://www.cloudflare.com/ips-v4
# https://www.cloudflare.com/ips-v6
variable "cloudflare_ip_addresses" {
  type = list
  default = ["173.245.48.0/20",
    "103.21.244.0/22",
    "103.22.200.0/22",
    "103.31.4.0/22",
    "141.101.64.0/18",
    "108.162.192.0/18",
    "190.93.240.0/20",
    "188.114.96.0/20",
    "197.234.240.0/22",
    "198.41.128.0/17",
    "162.158.0.0/15",
    "104.16.0.0/12",
    "172.64.0.0/13",
    "131.0.72.0/22",
    "2400:cb00::/32",
    "2606:4700::/32",
    "2803:f800::/32",
    "2405:b500::/32",
    "2405:8100::/32",
    "2a06:98c0::/29",
  "2c0f:f248::/32"]
}
